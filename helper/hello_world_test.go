package helper

import (
	"fmt"
	"runtime"
	"testing"

	"guru.com/src"

	"github.com/stretchr/testify/require"
)

func TestHelloWorld(t *testing.T) {
	result := src.HelloWorld("Aditira")
	if result != "Hello Aditira" {
		// unit test failed
		// panic("Result is not Hello Aditira")
		t.Error("Harusnya Hello Aditira") //-> Script selanjutnya dieksekusi
		// t.Fatal("Harusnya Hello Aditira") //-> Script selanjutnya tidak dieksekusi
	}

	fmt.Println("Dieksekusi")
}

func TestHelloWorld2(t *testing.T) {
	result := src.HelloWorld("Aditira")
	// assert.Equal(t, "Hello Aditira", result)
	require.Equal(t, "Hello Aditira", result)
	fmt.Println("Dieksekusi")
}

func TesSkip(t *testing.T) {
	if runtime.GOOS == "darwin" {
		t.Skip("Unit test tidak bisa berjalan di Mac")
	}

	result := src.HelloWorld("Aditira")
	require.Equal(t, "Hello Aditira", result)
}
